import cv2 as cv
import numpy as np


class Solver:
    def __init__(self):
        """
        class with implementations needed methods for reading img, binarization and find contours

        self.__img: None
        self.__result: None
        """
        self.__img = None
        self.__result = None

    def read_img(self, path):
        """
        self.__img: np.array

        :param path: str (path to the image on the disk)
        :return: None
        """
        img = cv.imread(path)

        if img.shape[2] == 3:
            img = cv.cvtColor(img, cv.COLOR_RGB2GRAY)

        self.__img = img

    def show_img(self):
        if self.__img is None:
            raise ValueError("Read img by first step!")

        cv.imshow("image", self.__img)
        cv.waitKey(0)

        cv.destroyAllWindows()

    def binarization(self):
        """
        computes binarization threshold with otsu method for smoothed image and fill self.__result with result image

        self.__result = np.array

        :return: None
        """
        if self.__img is None:
            raise ValueError("Read img by first step!")

        cv.medianBlur(src=self.__img, ksize=3, dst=self.__img)

        hist = cv.calcHist([self.__img], [0], None, [256], [0, 256])
        hist_norm = hist.ravel() / hist.max()
        Q = hist_norm.cumsum()

        bins = np.arange(256)

        fn_min = np.inf

        for i in range(1, 256):
            p1, p2 = np.hsplit(hist_norm, [i])  # probabilities
            q1, q2 = Q[i], Q[255] - Q[i]  # cum sum of classes
            b1, b2 = np.hsplit(bins, [i])  # weights

            # finding means and variances
            m1, m2 = np.sum(p1 * b1) / q1, np.sum(p2 * b2) / q2
            v1, v2 = np.sum(((b1 - m1) ** 2) * p1) / q1, np.sum(((b2 - m2) ** 2) * p2) / q2

            # calculates the minimization function
            fn = v1 * q1 + v2 * q2
            if fn < fn_min:
                fn_min = fn

        # find otsu's threshold value with OpenCV function
        ret, otsu = cv.threshold(self.__img, 0, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
        self.__result = otsu

    def show_result(self):
        cv.imshow("result", np.hstack((self.__img, self.__result)))
        cv.waitKey(0)

        cv.destroyAllWindows()

    def find_thresh(self):
        """
        function calculates thresholds with Laplacian

        :return: None
        """
        self.__result = cv.Laplacian(self.__result, cv.CV_8U)