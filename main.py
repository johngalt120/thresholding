import plac

from graphics import Solver


def main(img_path: "path to the image for binarization"):
    solver = Solver()
    solver.read_img(img_path)
    solver.show_img()
    solver.binarization()
    solver.find_thresh()
    solver.show_result()


if __name__ == "__main__":
    plac.call(main)